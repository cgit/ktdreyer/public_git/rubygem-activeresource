# Generated from activeresource-2.3.17.gem by gem2rpm -*- rpm-spec -*-
%global gem_name activeresource
%if 0%{?el6}
%global rubyabi 1.8
%else
%global rubyabi 1.9.1
%endif

Summary: Think Active Record for web resources
Name: rubygem-%{gem_name}
Version: 2.3.17
Release: 1%{?dist}
Group: Development/Languages
License: MIT
URL: http://www.rubyonrails.org
Source0: %{gem_name}-%{version}.gem
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: rubygem(activesupport) = 2.3.17
BuildRequires: ruby(abi) = %{rubyabi}
%if 0%{?el6}
BuildRequires: ruby(rubygems)
%else
BuildRequires: rubygems-devel
%endif
BuildRequires: rubygem(activesupport)
BuildRequires: rubygem(mocha)
BuildArch: noarch
Provides: rubygem(%{gem_name}) = %{version}

# A test fails with 'Disallowed type attribute: "yaml"'
# Patch out this test.
Patch0: rubygem-activeresource-2.3.17-skip-yaml-test.patch

# macros for RHEL6 compatibility:
%{!?gem_dir: %global gem_dir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)}
%{!?gem_instdir: %global gem_instdir %{gem_dir}/gems/%{gem_name}-%{version}}
%{!?gem_libdir: %global gem_libdir %{gem_instdir}/lib}
%{!?gem_cache: %global gem_cache %{gem_dir}/cache/%{gem_name}-%{version}.gem}
%{!?gem_spec: %global gem_spec %{gem_dir}/specifications/%{gem_name}-%{version}.gemspec}
%{!?gem_docdir: %global gem_docdir %{gem_dir}/doc/%{gem_name}-%{version}}
%{!?gem_extdir: %global gem_extdir %{_libdir}/gems/exts/%{gem_name}-%{version}}

%description
Wraps web resources in model classes that can be manipulated through XML over
REST.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}

%prep
%setup -q -c -T
mkdir -p .%{gem_dir}
gem install --local --install-dir .%{gem_dir} \
            --force %{SOURCE0}

pushd .%{gem_instdir}
%patch0 -p2
popd

%build

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/

%check
pushd .%{gem_instdir}
  testrb test/*_test.rb
popd


%files
%dir %{gem_instdir}
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/README
%doc %{gem_instdir}/CHANGELOG
%{gem_instdir}/Rakefile
%{gem_instdir}/test


%changelog
* Wed Feb 20 2013 ktdreyer@ktdreyer.com - 2.3.17-1
- Initial package, created with gem2rpm 0.8.1
